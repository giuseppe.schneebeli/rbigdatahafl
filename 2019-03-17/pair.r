#Name of  path where the data are stored
path_to_mydata='../Data'
setwd(path_to_mydata)

#Name of the Sqlite data baase
name="REFDB3.db"

#Load package to read the data 
library("RSQLite")

#connect to the data base
con=dbConnect(RSQLite::SQLite(), dbname=name)

#Send a SQl query and get the results
#select randomly 10 data 
command=paste('SELECT * FROM Echoes ORDER BY RANDOM() LIMIT ',50,sep="")
Readdata = dbGetQuery( con,command )

dbDisconnect(con)
Readdata <- Readdata[-c(1:7)]
F15Data <- Readdata[c(1:5)]
F610Data <- Readdata[c(6:10)]
V15Data <- Readdata[c(11:15)]
V610Data <- Readdata[c(16:20)]

setwd('../2019-03-17')
pairs(F15Data)
pairs(F610Data)
pairs(V15Data)
pairs(V610Data)
library(TeachingDemos)
pairs2(F15Data,F610Data)
pairs2(F610Data,V15Data)
pairs2(V15Data,V610Data)
pairs2(F15Data,V15Data)
pairs2(F610Data,V610Data)
pairs2(F15Data,V610Data)
