#Name of  path where the data are stored
path_to_mydata='../Data'
setwd(path_to_mydata)

#Name of the Sqlite data baase
name="REFDB3.db"

#Load package to read the data 
library("RSQLite")

#connect to the data base
con=dbConnect(RSQLite::SQLite(), dbname=name)

#Send a SQl query and get the results
#select randomly 10 data 
command=paste('SELECT * FROM Echoes ORDER BY RANDOM() LIMIT ',50,sep="")
Readdata = dbGetQuery( con,command )

dbDisconnect(con)

#Read one feature
F1=Readdata$F1
#or another one
V1=Readdata$V1

setwd("../2019-03-13")
jpeg("Rplot.jpg")
plot(F1,V1,pch=4,cex=2)

