#Name of  path where the data are stored
path_to_mydata='../Data'
setwd(path_to_mydata)

#Name of the Sqlite data baase
name="REFDB3.db"

#Load package to read the data 
library("RSQLite")

#connect to the data base
con=dbConnect(RSQLite::SQLite(), dbname=name)

#Send a SQl query and get the results
#select randomly 10 data 
command=paste('SELECT * FROM Echoes ORDER BY RANDOM() LIMIT ',50,sep="")
Readdata = dbGetQuery( con,command )

dbDisconnect(con)
Readdata <- Readdata[-c(1:7)]
M<-cor(Readdata)
head(M)

setwd('../2019-03-14')
library(corrplot)
col1 <- colorRampPalette(c("#67001F", "#B2182B", "#D6604D", "#F4A582", "#FDDBC7", "#FFFFFF", "#D1E5F0", "#92C5DE", "#4393C3", "#2166AC", "#053061"))
corrplot(M, method = "square",col=col1(100))
