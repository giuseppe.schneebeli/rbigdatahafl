#Name of  path where the data are stored
path_to_mydata='../Data'
setwd(path_to_mydata)

#Name of the Sqlite data baase
name="REFDB3.db"

#Load package to read the data 
library("RSQLite")
#F6/F,F6/F9 und F5/F10
#connect to the data base
con=dbConnect(RSQLite::SQLite(), dbname=name)

#Send a SQl query and get the results
#select randomly 10 data 
command=paste('SELECT * FROM Echoes ORDER BY RANDOM() LIMIT ',100,sep="")
Readdata = dbGetQuery( con,command )

dbDisconnect(con)
Readdata <- Readdata[-c(1:3)]
hours <- paste(Readdata$Hour)
minutes <- paste(Readdata$Minute)
secounds <- paste(Readdata$Second)
time <- paste(hours,minutes,secounds, sep=':')
time <- strptime(time, format='%H:%M:%S')
Readdata$time <- time
head(Readdata)
setwd('../2019-03-18')
library(TeachingDemos)
pairs2(Readdata$time, Readdata$F1)
