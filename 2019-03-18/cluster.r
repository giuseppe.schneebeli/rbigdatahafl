#Name of  path where the data are stored
path_to_mydata='../Data'
setwd(path_to_mydata)

#Name of the Sqlite data baase
name="REFDB3.db"

#Load package to read the data 
library("RSQLite")
#F6/F,F6/F9 und F5/F10
#connect to the data base
con=dbConnect(RSQLite::SQLite(), dbname=name)

#Send a SQl query and get the results
#select randomly 10 data 
command=paste('SELECT * FROM Echoes ORDER BY RANDOM() LIMIT ',100,sep="")
Readdata = dbGetQuery( con,command )

dbDisconnect(con)
F6F9=data.frame(xcoor=Readdata$F6,ycoor=Readdata$F9)#, zcoor=z)
F5F10=data.frame(xcoor=Readdata$F5,ycoor=Readdata$F10)#, zcoor=z)
colnames(F6F9) <- c("F6","F9")
colnames(F5F10) <- c("F6","F9")
setwd('../2019-03-18')
#library(NbClust)
#set.seed(1234)
#nc <- NbClust(data, min.nc=2, max.nc=15, method="kmeans")
#table(nc$Best.n[1,])

library(flexclust)

c1<-cclust(F6F9, 2, dist="euclidean", method="neuralgas")
c2<-cclust(F5F10, 2, dist="euclidean", method="neuralgas")

#Results are
c1@centers      # centers of teh clusters
c1@cluster      # data attributaion to each clusters

pairs(F6F9, col=predict(c1))
pairs(F5F10, col=predict(c2))
